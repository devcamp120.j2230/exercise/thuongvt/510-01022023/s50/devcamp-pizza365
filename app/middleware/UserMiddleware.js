const getAllUserMiddleware = (req,res,next)=>{ 
    console.log("get all User");
    next()
}

const getAUserMiddleware = (req,res,next)=>{
    console.log("get a User");
    next();
}

const postUserMiddleware = (req,res,next)=>{
    console.log("post a User");
    next();
}

const putUserMiddleware = (req,res,next)=>{
    console.log("put a User");
    next();
}

const deleteUserMiddleware = (req,res,next)=>{
    console.log("delete a User");
    next();
}
const getUserLimitMiddleware = (req,res,next)=>{ 
    console.log("get User limit");
    next()
}
const getUserSkipMiddleware = (req,res,next)=>{ 
    console.log("get User skip");
    next()
}
const getUserSortMiddleware = (req,res,next)=>{ 
    console.log("get User sort");
    next()
}
const getUserLimitSkipMiddleware = (req,res,next)=>{ 
    console.log("get User limit skip");
    next()
}
const getUserSortLimitSkipMiddleware = (req,res,next)=>{ 
    console.log("get User Sort limit skip");
    next()
}
module.exports = {
    getAllUserMiddleware,
    getAUserMiddleware,
    putUserMiddleware,
    postUserMiddleware,
    deleteUserMiddleware,
    getUserLimitMiddleware,
    getUserSkipMiddleware,
    getUserSortMiddleware,
    getUserLimitSkipMiddleware,
    getUserSortLimitSkipMiddleware
}
