//Khai báo thư viện express
const express = require("express");
// Khai báo Middleware 
const {
    getAllDrinkMiddleware,
    getADrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
} =  require (`../middleware/DrinkMiddleware`);

//Khai báo controller
const {
    createDrink,
    getAllDrink,
    getADrinkById,
    updateDrinkById,
    deleteDrinkById
} = require("../controller/DrinkController")

// Tạo router
const DrinkRouter = express.Router();

// Sử dụng router
DrinkRouter.get("/Drink",getAllDrinkMiddleware,getAllDrink);

DrinkRouter.get("/Drink/:DrinkId", getADrinkMiddleware,getADrinkById)

DrinkRouter.post("/Drink",postDrinkMiddleware,createDrink)

DrinkRouter.put("/Drink/:DrinkId",putDrinkMiddleware,updateDrinkById)

DrinkRouter.delete("/Drink/:DrinkId",deleteDrinkMiddleware,deleteDrinkById)

module.exports = {DrinkRouter}
